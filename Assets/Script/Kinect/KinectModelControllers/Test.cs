﻿using UnityEngine;
using System.Collections;
using System;

public class Test : MonoBehaviour {

    public Transform t1, t2;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {





        Vector3 dir = t1.localEulerAngles;
        dir = new Vector3(dir.x, 0, dir.z);
        dir = dir.normalized;
        float f = Vector3.Cross(dir, Vector3.right).magnitude;

        double d = (double)f;

        float _angle = (float)Math.Acos(f);

        t1.Rotate(Vector3.up, _angle);


        dir = t2.eulerAngles;
        dir = new Vector3(dir.x, 0, dir.z);
        dir = dir.normalized;
         f = Vector3.Cross(dir, t1.right).magnitude;

         d = (double)f;

        _angle = (float)Math.Acos(f);

        t2.Rotate(Vector3.up, _angle);

    }
}
