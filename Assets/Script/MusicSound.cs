﻿using UnityEngine;
using System.Collections;

public class MusicSound : MonoBehaviour {

    AudioSource audioSuorce;

	// Use this for initialization
	void Start () {
        audioSuorce = GetComponent<AudioSource>();
    }
	
    public void MyPlay()
    {
        audioSuorce.Play();
    }
      
}
