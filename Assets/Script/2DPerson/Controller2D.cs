﻿using UnityEngine;
using System.Collections;

public class Controller2D : MonoBehaviour
{


    public float badValueAngle = 40;

    public SkeletonWrapper sw;

    public GameObject HipCenter;
    public GameObject Spine;
    public GameObject ShoulderCenter;
    public GameObject Head;
    public GameObject ShoulderLeft;
    public GameObject ElbowLeft;
    public GameObject WristLeft;
    public GameObject HandLeft;
    public GameObject ShoulderRight;
    public GameObject ElbowRight;
    public GameObject WristRight;
    public GameObject HandRight;
    public GameObject HipLeft;
    public GameObject KneeLeft;
    public GameObject AnkleLeft;
    public GameObject FootLeft;
    public GameObject HipRight;
    public GameObject KneeRight;
    public GameObject AnkleRight;
    public GameObject FootRight;

    public int player = 0;
    public float lerpValue = 0.1f;
    GameObject[] bones;

    Quaternion[] lastRotations;

    public Transform parentRotation;


    // Use this for initialization
    void Start()
    {
        bones = new GameObject[(int)Kinect.NuiSkeletonPositionIndex.Count] {
        HipCenter ,
        Spine,
        ShoulderCenter,
        Head ,
        ShoulderLeft,
        ElbowLeft ,
        WristLeft ,
        HandLeft ,
        ShoulderRight,
        ElbowRight ,
        WristRight ,
        HandRight ,
        HipLeft ,
        KneeLeft ,
        AnkleLeft ,
        FootLeft ,
        HipRight ,
        KneeRight,
        AnkleRight,
        FootRight,
        }
       ;

        lastRotations = new Quaternion[bones.Length];

        /*  count = 20


        HipCenter = 0,
        Spine = 1,
        ShoulderCenter = 2,
        Head = 3,
        ShoulderLeft = 4,
        ElbowLeft = 5,
        WristLeft = 6,
        HandLeft = 7,
        ShoulderRight = 8,
        ElbowRight = 9,
        WristRight = 10,
        HandRight = 11,
        HipLeft = 12,
        KneeLeft = 13,
        AnkleLeft = 14,
        FootLeft = 15,
        HipRight = 16,
        KneeRight = 17,
        AnkleRight = 18,
        FootRight = 19,
        */

    }

    void Rotate(int i)
    {

        //float k = (t.y <= 90 && t.y >= -90) ? 0 : 180;
        // t = new Vector3(t.x, k, t.z);


        lastRotations[i] = bones[i].transform.localRotation;

        //Quaternion t = sw.boneAbsoluteOrientation[player, i];
        //bones[i].transform.rotation = t;

        Vector3 t = sw.boneAbsoluteOrientation[player, i].eulerAngles;
        //float k = (t.y <= 90 && t.y >= -90) ? 0 : 180;
        float k = t.y;
        bool badValues = false;

        /*if ((t.y <= 40 && t.y >= -40))
        {
            k = 0;
        }
        else
        {
            if (t.y <= 180 + 40 && t.y >= 180 - 40)
            {
                k = 180;

            }
            else
            {
                badValues = true;
            }
        }*/

        if (!badValues)
        {
            t = new Vector3(t.x, k, t.z);
            bones[i].transform.rotation = Quaternion.Euler(t);

            //righ legs
            if (i == 16 || i == 17 || i == 18 || i == 19)
            {
                bones[i].transform.Rotate(Vector3.forward, 90);

                bones[i].transform.Rotate(Vector3.right, 180);

                //bones[i].transform.Rotate(Vector3.forward, 180);

            }
            //right arms
            if (i == 8 || i == 9 || i == 10 || i == 11)
            {
                bones[i].transform.Rotate(Vector3.forward, 90);

                //

                bones[i].transform.Rotate(Vector3.forward, 180);
            }
            //left legs
            if ( i == 12 || i == 13 || i == 14 || i == 15)
            {
                bones[i].transform.Rotate(Vector3.forward, -90);

                bones[i].transform.Rotate(Vector3.right, 180);

                //bones[i].transform.Rotate(Vector3.up, 180);

               

            }
            //left arms
            if (i == 4 || i == 5 || i == 6 || i == 7 )
            {
                bones[i].transform.Rotate(Vector3.forward, -90);

                bones[i].transform.Rotate(Vector3.up, 180);
            }

            if (i == 0 || i == 1 || i == 2 || i == 3)
            {
                bones[i].transform.Rotate(Vector3.forward, -90);
            }


            if (i == (int)Kinect.NuiSkeletonPositionIndex.HipLeft)
            {
                bones[i].transform.Rotate(Vector3.up, 180);
            }

            if (i == (int)Kinect.NuiSkeletonPositionIndex.Spine + 1)
            {
                bones[i].transform.Rotate(bones[i].transform.right, -40);
            }

        }

    }

    void SecondRotate(int i)
    {
        Quaternion t = bones[i].transform.localRotation;
        bones[i].transform.localRotation = Quaternion.Lerp(lastRotations[i], t, lerpValue); //sw.boneAbsoluteOrientation[player, i];


        if (i == (int)Kinect.NuiSkeletonPositionIndex.HipCenter)
        {
            if (parentRotation != null)
            {
                bones[i].transform.localRotation = bones[i].transform.localRotation * parentRotation.localRotation;

            }

        }
    }


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i] != null)
            {
                Rotate(i);
                //Vector3 v3 = new Vector3(-sw.bonePos[player, 0].y// jump
                //     , sw.bonePos[player, 0].x, //left -right
                //     0); // forward -sw.bonePos[player, 0].z
                // bones[0].transform.localPosition = v3 * 3;
            }

        }
        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i] != null)
            {
                SecondRotate(i);
            }

        }

    }

}
