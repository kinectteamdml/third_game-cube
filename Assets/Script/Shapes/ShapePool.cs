﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShapePool : MonoBehaviour {
    private static ShapePool instance_;
    public static ShapePool Instance
    {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<ShapePool>();
            return instance_;
        }
    }

    public GameObject[] prefabs;
    Stack<GameObject> pool = new Stack<GameObject>();
    
    public GameObject GetFromPool()
    {
        if (pool.Count > 0)
        {
            GameObject go = pool.Pop();
            go.SetActive(true);
            return go;
        }
        else
        {
            GameObject temp = Instantiate(prefabs[Random.Range(0, prefabs.Length)]);
            temp.transform.SetParent(transform);
            return temp;
        }
    }

    public void PutToPool(GameObject go)
    {
        pool.Push(go);
        go.SetActive(false);
    }
}
