﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class HardMode : MonoBehaviour {

    Text text;
    string startText;
    public PlatesController pContr;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        startText = text.text;
    }
	
	// Update is called once per frame
	void Update () {


        text.text = startText + pContr.hard;

    }
}
