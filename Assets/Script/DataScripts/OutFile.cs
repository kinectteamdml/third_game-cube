﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.Collections.Generic;
using System;
using System.IO;
[XmlRoot("Params")]
public class OutFile
{
    [XmlAttribute("PatientID")]
    public int patientID = 0;
    [XmlAttribute("StartTime")]
    public string startTime = "";
    [XmlAttribute("EndTime")]
    public string endTime = "";
    [XmlAttribute("FinalScore")]
    public int finalScore = 0;

    [XmlArray("Tasks")]
    [XmlArrayItem("Task")]
    public List<Task> tasks = new List<Task>();


    public OutFile()
    {
    }

    public void Save(string name)
    {
        String path = Application.dataPath + "/../out/" + name + ".xml";
        Debug.Log(path);
        XmlSerializer serializer = new XmlSerializer(typeof(OutFile));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }

    public OutFile Load(string name)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(OutFile));
        try
        {
            Debug.Log(Application.persistentDataPath);
            using (var stream = new FileStream(Application.persistentDataPath + "/" + name + ".xml", FileMode.Open))
            {
                return serializer.Deserialize(stream) as OutFile;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
            return new OutFile();
        }
    }

}
