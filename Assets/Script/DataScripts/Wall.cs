﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System;

[Serializable]
public class Wall {

    [XmlAttribute("WallName")]
    public string name;
    [XmlAttribute("Index1")]
    public int index1;
    [XmlAttribute("Index2")]
    public int index2;

    public Wall()
    {

    }
    public Wall(string _name,int i1,int i2)
    {
        name = _name;
        index1 = i1;
        index2 = i2;
    }
	
}
