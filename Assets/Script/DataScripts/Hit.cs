﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System;
[Serializable]
public class Hit {
    [XmlAttribute("Joint")]
    public string joint;
    [XmlAttribute("Type")]
    public int type;
    [XmlAttribute("TimeStamp")]
    public string timeStamp;
    [XmlAttribute("X")]
    public float x;
    [XmlAttribute("Y")]
    public float y;
    [XmlAttribute("Z")]
    public float z;

    public Hit(){ 
    }

    public Hit(string joint_, Vector3 position_, int type_, string timeStamp_)
    {
        this.joint = joint_;
        this.x = position_.x;
        this.y = position_.y;
        this.z = position_.z;
        this.type = type_;
        this.timeStamp = timeStamp_;
    }

    public Hit(string joint_, float x_, float y_, float z_, int type_, string timeStamp_)
    {
        this.joint = joint_;
        this.x = x_;
        this.y = y_;
        this.z = z_;
        this.type = type_;
        this.timeStamp = timeStamp_;
    }
}
