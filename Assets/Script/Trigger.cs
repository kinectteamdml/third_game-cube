﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trigger : MonoBehaviour
{

    public Plate myPlate;
    public int index;

    Material onStart;
    public List<Collider> usedColliders;
    

    // Use this for initialization
    void Start()
    {
        onStart = myPlate.transform.GetChild(index).GetComponent<MeshRenderer>().material;
        usedColliders = new List<Collider>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerStay  (Collider other)
    {


        if (other.tag == myPlate.myTag )
        {
            if (usedColliders.Count==0 && !myPlate.usedColliders.Contains(other))
            {
                //Debug.Log("stay");

                myPlate.PlateOn = true;

                myPlate.activeObjectsNumber++;

                //myPlate.transform.GetChild(index).GetComponent<MeshRenderer>().material = myPlate.active;
                myPlate.transform.GetChild(index).GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", new Color(1.8f, 1.8f, 1.8f,1)); 
                // Windowmat.SetColor("_EmissionColor", Color.black);


                usedColliders.Add(other);
                myPlate.usedColliders.Add(other);
            } 



        }

    }
    void OnTriggerExit(Collider other)
    {

        if (other.tag == myPlate.myTag )
        {
            if (usedColliders.Contains(other))
            {

                myPlate.PlateOn = false;

                myPlate.activeObjectsNumber--;

                //myPlate.transform.GetChild(index).GetComponent<MeshRenderer>().material = onStart;
                myPlate.transform.GetChild(index).GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", new Color(0.01f, 0.01f, 0.01f, 1));

                usedColliders.Remove(other);
                myPlate.usedColliders.Remove(other);

            }
        }

    }

}
