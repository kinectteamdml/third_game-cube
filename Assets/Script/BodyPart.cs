﻿using UnityEngine;
using System.Collections;

public class BodyPart : MonoBehaviour
{


    public Transform target;
    public bool inverse = false;
    public Transform root;

    public bool IK = false;
    public bool needIK = false;
    public Vector3 ikPos;
    public Transform ikTarget;
    public float delta;
    public float ikLenght = 3;

    KinectPointController KPC;

    public Transform MyPlace;

    public float lerpValue = 1;

    IEnumerator SelectObjectInBody()
    {
        yield return new WaitForSeconds(0.1f);


    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        int k = 1;
        k = inverse ? -k : k;

        if (needIK)
        {
            Vector3 dir = MyPlace.position - root.position;
            Ray ray = new Ray(root.position, dir);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, dir.magnitude))
            {
                Debug.DrawLine(MyPlace.position, root.position, Color.red);
                ikPos = hit.point + hit.normal * delta;


                transform.position = Vector3.Lerp(transform.position, ikPos, lerpValue * Time.deltaTime);

                Quaternion lastR = transform.rotation;
                ///Change Rotation
                Vector3 normal = -hit.normal;
                transform.forward = normal;


                transform.RotateAround(normal.normalized, Mathf.PI / 2); //  position of the object in 12 hours

                int onForward = 1;
                onForward = root.position.z <= ikPos.z ? 1 : -1; // if hit behind take "-angle"

                Vector3 arrow = ikPos - root.position;
                arrow = new Vector3(0, arrow.y, arrow.z); // projection on YZ

                float angle = Vector3.Angle(Vector3.up, arrow);
                angle = 2 * Mathf.PI * angle / 360 * onForward;

                transform.RotateAround(normal, angle);
                Debug.Log(angle);
                ///
                Quaternion newR = transform.rotation;
                transform.rotation = Quaternion.Lerp(lastR, newR, lerpValue * Time.deltaTime);

            }
            else
            {


                dir = (target.position - MyPlace.position).normalized * ikLenght;
                ray = new Ray(MyPlace.position, dir);

                if (Physics.Raycast(ray, out hit, dir.magnitude))
                {
                    //Debug.DrawLine(MyPlace.position, MyPlace.position+dir, Color.green);
                    Debug.DrawRay(MyPlace.position, dir, Color.green);
                    ikPos = hit.point + hit.normal * delta;


                    transform.position = Vector3.Lerp(transform.position, ikPos, lerpValue * Time.deltaTime);

                    Quaternion _lastR = transform.rotation;
                    ///Change Rotation
                    Vector3 normal = -hit.normal;
                    transform.forward = normal;


                    transform.RotateAround(normal.normalized, Mathf.PI / 2); //  position of the object in 12 hours

                    int onForward = 1;
                    onForward = root.position.z <= ikPos.z ? 1 : -1; // if hit behind take "-angle"

                    Vector3 arrow = ikPos - MyPlace.position;
                    arrow = new Vector3(0, arrow.y, arrow.z); // projection on YZ

                    float angle = Vector3.Angle(Vector3.up, arrow);
                    angle = 2 * Mathf.PI * angle / 360 * onForward;

                    transform.RotateAround(normal, angle);
                    Debug.Log(angle);
                    ///
                    Quaternion _newR = transform.rotation;
                    transform.rotation = Quaternion.Lerp(_lastR, _newR, lerpValue * Time.deltaTime);

                }
                else
                {
                    transform.position = Vector3.Lerp(transform.position, MyPlace.position, Time.deltaTime * lerpValue);


                    Quaternion lastR = transform.rotation;
                    ///Change Rotation
                    transform.right = (target.position - transform.position).normalized * k;
                    ///
                    Quaternion newR = transform.rotation;
                    transform.rotation = Quaternion.Lerp(lastR, newR, lerpValue * Time.deltaTime);
                }




            }
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, MyPlace.position, Time.deltaTime * lerpValue);


            Quaternion lastR = transform.rotation;
            ///Change Rotation
            transform.right = (target.position - transform.position).normalized * k;
            ///
            Quaternion newR = transform.rotation;
            transform.rotation = Quaternion.Lerp(lastR, newR, lerpValue * Time.deltaTime);
        }

        








       /*f (!IK)
        {
            //transform.position = MyPlace.position;
            transform.position = Vector3.Lerp(transform.position, MyPlace.position, Time.deltaTime * lerpValue);

            transform.right = (target.position - transform.position).normalized * k;
        }
        else
        {
            Vector3 normal = ikTarget.right;
            transform.forward = normal;
            transform.Rotate(ikTarget.forward, 90);



        }*/

    }
}
